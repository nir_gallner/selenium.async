package co.verisoft.selenium.async.tests;

import org.javatuples.Pair;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;
import co.verisoft.selenium.async.AsyncListener;
import co.verisoft.selenium.async.AsyncTask;
import co.verisoft.selenium.async.Observer;
import co.verisoft.selenium.async.SeleniumTask;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import sun.jvm.hotspot.debugger.cdbg.basic.BasicSym;

import java.io.File;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.*;

public class AsyncTest {

    private WebDriver driver;
    private AsyncListener asyncListener;

    private static final String pageTestUrl = "file://"
            + new File(System.getProperty("user.dir") + "/src/test/resources/TestPage.html")
            .getAbsolutePath();

    @BeforeAll
    private static void beforeAll() {

        WebDriverManager.chromedriver().setup();

    }

    @BeforeEach
    private void beforeEach() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);

        asyncListener = new AsyncListener();
        driver = new EventFiringWebDriver(new ChromeDriver(options));
        ((EventFiringWebDriver)driver).register(asyncListener);

        driver.get(pageTestUrl);
    }

    @AfterEach
    public void afterEach(){
        driver.close();
    }

    @Test
    public void shouldInvokeAsyncOperationOnceAfterFindBy() throws InterruptedException {

        // Set the async task
        Observer o = new AsyncTask(driver, new SeleniumTask() {
            @Override
            public boolean doTask() {
                driver.findElement(By.id("texta")).sendKeys("test1");
                return true;
            }
        });
        asyncListener.register(o);

        // Default dispatch is 1 second
        Thread.sleep(1100);
        driver.findElement(By.id("texta"));
        String text = (String) ((JavascriptExecutor)driver).executeScript("return document.getElementById('texta').value");
        assertEquals("test1", text, "text should have been " + "test1");
    }

    @Test
    public void shouldNotBeInvokedTwice() throws InterruptedException {

        // Set the async task
        Observer o = new AsyncTask(driver, new SeleniumTask() {
            @Override
            public boolean doTask() {
                driver.findElement(By.id("texta")).sendKeys("test1");
                return true;
            }
        });
        asyncListener.register(o);

        // First invocation - default dispatch is 1 second
        Thread.sleep(1100);
        driver.findElement(By.id("texta"));
        String text = (String) ((JavascriptExecutor)driver).executeScript("return document.getElementById('texta').value");
        assertEquals("test1", text, "text should have been " + "test1");

        // Change value
        ((JavascriptExecutor)driver).executeScript("document.getElementById('texta').value='delete'");

        // Wait and try again
        Thread.sleep(1100);
        driver.findElement(By.id("texta"));
        text = (String) ((JavascriptExecutor)driver).executeScript("return document.getElementById('texta').value");
        assertNotEquals(text, "test1", "text should have been " + "test1");
    }

    @Test
    public void shouldBeInvokedTwice() throws InterruptedException {

        // Set async task
        Observer o = new AsyncTask(driver, new SeleniumTask() {
            @Override
            public boolean doTask() {
                driver.findElement(By.id("texta")).sendKeys("test1");
                return false;
            }
        });
        asyncListener.register(o);

        // First invocation - default dispatch is 1 second
        Thread.sleep(1100);
        driver.findElement(By.id("texta"));
        String text = (String) ((JavascriptExecutor)driver).executeScript("return document.getElementById('texta').value");
        assertEquals("test1", text, "text should have been " + "test1");

        // Change value
        ((JavascriptExecutor)driver).executeScript("document.getElementById('texta').value=''");

        // Wait and try again
        Thread.sleep(1100);
        driver.findElement(By.id("texta"));
        text = (String) ((JavascriptExecutor)driver).executeScript("return document.getElementById('texta').value");
        assertEquals("test1", text, "text should have been " + "test1");

    }

    @Test
    public void shouldChangeDispatchValues() {
        AsyncListener listener = new AsyncListener();
        Pair<Integer, ChronoUnit> interval = listener.getDispatchInterval();

        // Test default values
        assertEquals(1, interval.getValue0(), "default value for interval is 1");
        assertEquals(ChronoUnit.SECONDS, interval.getValue1(), "Default value should be seconds");

        // Change values
        listener.setDispatchInterval(3, ChronoUnit.DAYS);
        interval = listener.getDispatchInterval();

        assertEquals(3, interval.getValue0(), "Value was changed to 3");
        assertEquals(ChronoUnit.DAYS, interval.getValue1(), "Value was changed to days");
    }

    @Test
    public void shouNotAllowDispatchValuesToBeLessThanOneSecond() {
        AsyncListener listener = new AsyncListener();
        Pair<Integer, ChronoUnit> interval = listener.getDispatchInterval();

        // Test default values
        assertEquals(1, interval.getValue0(), "default value for interval is 1");
        assertEquals(ChronoUnit.SECONDS, interval.getValue1(), "Default value should be seconds");

        // Change values
        listener.setDispatchInterval(0, ChronoUnit.SECONDS);
        interval = listener.getDispatchInterval();

        assertEquals(1, interval.getValue0(), "Value was changed to 3");
        assertEquals(ChronoUnit.SECONDS, interval.getValue1(), "Value was changed to days");

        // Change values
        listener.setDispatchInterval(3, ChronoUnit.MILLIS);
        interval = listener.getDispatchInterval();

        assertEquals(1, interval.getValue0(), "Value was changed to 3");
        assertEquals(ChronoUnit.SECONDS, interval.getValue1(), "Value was changed to days");

    }

    @Test
    public void DoesNotCrashWhenUnregisterNoObservers() {
        Observer o = new Observer() {
            @Override
            public void update() {
                return;
            }

            @Override
            public boolean isDisposed() {
                return false;
            }
        };

        assertDoesNotThrow(() -> asyncListener.unregister(o), "Should not throw an exception");
    }

    @Test
    public void shouldBeInvokedAfterDispatchChange() throws InterruptedException {

        asyncListener.setDispatchInterval(3, ChronoUnit.SECONDS);
        Observer o = new AsyncTask(driver, new SeleniumTask() {
            @Override
            public boolean doTask() {
                driver.findElement(By.id("texta")).sendKeys("test1");
                return false;
            }
        });
        asyncListener.register(o);

        // Wait insufficient time - should not be invoked
        Thread.sleep(500);
        driver.findElement(By.id("texta"));
        String text = (String) ((JavascriptExecutor)driver).executeScript("return document.getElementById('texta').value");
        assertEquals("", text, "text should have been ''");

        // Wait and try again - should  be invoked
        Thread.sleep(3000);
        driver.findElement(By.id("texta"));
        text = (String) ((JavascriptExecutor)driver).executeScript("return document.getElementById('texta').value");
        assertEquals("test1", text, "text should have been test1");
    }

    @Test
    public void shouldNotAllowToCreateTaskWithLessThanOneSecond()  {

        // The default
        AsyncListener listener = new AsyncListener(1, ChronoUnit.SECONDS);
        assertEquals("1", listener.getDispatchInterval().getValue0().toString(), "Value should be 1");
        assertEquals(ChronoUnit.SECONDS, listener.getDispatchInterval().getValue1(), "Value should be SECONDS");

        // more than 1
        listener = new AsyncListener(10, ChronoUnit.SECONDS);
        assertEquals("10", listener.getDispatchInterval().getValue0().toString(), "Value should be 10");
        assertEquals(ChronoUnit.SECONDS, listener.getDispatchInterval().getValue1(), "Value should be SECONDS");

        // more than 1
        listener = new AsyncListener(10, ChronoUnit.DAYS);
        assertEquals("10", listener.getDispatchInterval().getValue0().toString(), "Value should be 10");
        assertEquals(ChronoUnit.DAYS, listener.getDispatchInterval().getValue1(), "Value should be Days");

        // less than 1 - resolve to 1
        listener = new AsyncListener(0, ChronoUnit.SECONDS);
        assertEquals("1", listener.getDispatchInterval().getValue0().toString(), "Value should be 1");
        assertEquals(ChronoUnit.SECONDS, listener.getDispatchInterval().getValue1(), "Value should be SECONDS");

        // less than 1 - resolve to 1
        listener = new AsyncListener(10, ChronoUnit.MILLIS);
        assertEquals("1", listener.getDispatchInterval().getValue0().toString(), "Value should be 1");
        assertEquals(ChronoUnit.SECONDS, listener.getDispatchInterval().getValue1(), "Value should be SECONDS");

    }

}
