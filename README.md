**Selenium.Async - An asynchronous feature for selenium**

The use case
------------

Say you need to automate a web site. And the web site has a popup which appears asynchronously after an ammount of time. 
You cannot ignore the popup, since it will sooner or later will block your script.
So you have one of 2 choises:
1. Wait for the popup, and block the execution of the script
2. Run an `asyncjs` using WebDriver `JavaScriptExecutor`

Since option 2, unforunately runs the script asynchronously on the web page ***but still blocks selenium***, we are left with
only one choice, really, which is wait and block other execution until the popup is dismissed.

---

## Selelnium is a single thread application, so is javascript

The main constraint is that Selenium WebDriver is a single thread application. It does not work with multi-thread. 
So how can we run asynchronous tests? Well, Javascript is a single thread as well. Javascript has an internal mechanism
for running on the same thread async actions. Async, not parallel.
In order to run async, we need to hook in to places in the execution. 
Luckily, we have `EventFiringWebDriver` exactly for this point.

---

## How does Selenium.Async works?

We hooked in to the most basic commant a standard selenium script does - search for elements (`findElement` and `findElements`). So the basic concept would be:
1. Define an async piece of code.
2. Register it to be executed.
3. Perform a regular selenium script which involves `findElement` and/or `findElements`.

***That's it....***

The feature is heavily documented, you can check it out. Take a look at the unit tests for gettings started examples



## Examples

### Example 1 - register the listener with the driver
```
WebDriver driver;
    
asyncListener = new AsyncListener();
driver = new EventFiringWebDriver(new FirefoxDriver());
((EventFiringWebDriver)driver).register(asyncListener);
```

### Example 2 - print the title of the page async to the display

```
AsyncListener listener = new AsyncListener();
listener.setDispatchInterval(2, ChronoUnit.SECONDS);
((EventFiringWebDriver)driver).register(listener);

Observer o = new AsyncTask(driver, new SeleniumTask() {

@Override
public boolean doTask() {
    String pageTitle = driver.getCurrentUrl();
    System.out.println("Time is " + LocalTime.now() + ", page url is " + pageTitle);
    return false;
}
});

listener.register(o);
```

### Example 3 - Wait for a pop up and dismiss it

```
AsyncListener listener = new AsyncListener();
listener.setDispatchInterval(2, ChronoUnit.SECONDS);
((EventFiringWebDriver)driver).register(listener);

Observer o = new AsyncTask(driver, new SeleniumTask() {
    @Override
    public boolean doTask() {
        boolean result = false;
 
        List<WebElement> elements = driver.findElements(By.id("popup-button-dismiss-locator"));
        if (elements.isEmpty())
            return false;
 
        try{
            elements.get(0).click();
            result = true;
        }
        catch(Throwable t){
            result = false;
        }
 
        return result;
    }
});

listener.register(o);
```